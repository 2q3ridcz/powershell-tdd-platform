# ツール開発はじめの一歩

powershell-tdd-platform を基にツール開発する際の一助として。  

## ドキュメント

Readmeは必ず書きましょう。  
ドキュメントの適正量はプロダクト毎に違うと思う。  
でもプロダクトの概要や使用方法を記載した Readmeは必須。  

参考：[人にやさしく。作ったものにはREADMEをつけよう。](https://think-simple-enjoy-life.com/599)


## バージョン番号

利用者への問い合わせ対応などに備え、バージョン番号をつけましょう。  
バージョン番号のつけ方に、統一ルールはない。  
こだわりなければ、オープンなルールの導入をオススメ。  
ルールを説明するドキュメントは用意されているし、  
後方互換性の有無を明確にするなどの基本を教えてくれる。

参考：[セマンティック バージョニング 2.0.0 | Semantic Versioning](https://semver.org/lang/ja/)


## コーディング

"./src/" 配下にコードを置きます。  
サンプルでは、以下の階層構造。

```
├─src
│  └─Fibbonaci
│      │  Fibbonaci.psm1
│      │
│      └─Functions
│              Get-Fibbonaci.psm1
```

外部からパッケージを読み込むときは、"./src/[パッケージ名]/[パッケージ名].psm1" を Import-Moduleする。  
これにより、"./src/[パッケージ名]/Functions/" 配下の関数を使用できるようになる。  
("./console_sample/console_sample.ps1" 参照。)  


## テスト

"./tests/" 配下にテストコードを置きます。  
"./tests_console/Start-Tests.ps1" でテストを実行します。  

テストフレームワークに Pesterを使用しています。  
Pesterは Windows 10 標準装備の Powershellに含まれます。(Version古いけど。)

参考：[PowerShell の Pester とは？](https://www.ipswitch.com/jp/blog/what-is-pester-for-powershell)

参考：[Should v3 · pester/Pester Wiki · GitHub](https://github.com/pester/Pester/wiki/Should-v3)

