﻿function Get-Fibbonaci {
    [CmdletBinding()]
    [OutputType([object[]])]
    param (
        [Parameter(Mandatory=$True)]
        [Int]
        $Max
    )

    begin {}

    process {
        Write-Warning -Message ("Parameter Max is not used! Max: " + $Max)
        return @(1,1,2,3,5,8,13)
    }

    end {}
}