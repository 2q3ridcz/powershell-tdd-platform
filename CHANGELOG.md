# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.2] - 2022-10-01
### Fixed
- Fix error which may be caused by Resolve-Path

### Changed
- Change coverage section in Readme.md to catch up with GitLab 15.0.

## [0.0.1] - 2022-01-15
### Added
- Initial release

[Unreleased]: https://gitlab.com/2q3ridcz/powershell-tdd-platform/-/compare/v0.0.2...main
[0.0.2]: https://gitlab.com/2q3ridcz/powershell-tdd-platform/-/tree/v0.0.1
[0.0.1]: https://gitlab.com/2q3ridcz/powershell-tdd-platform/-/tree/v0.0.1
