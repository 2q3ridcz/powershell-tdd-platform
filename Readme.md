# Powershell-TDD-Platform

Develop powershell scripts in a TDD way.

Powershell-TDD-Platform presents a directory tree, test tools, a dummy module, and sample tests for it.

## Usage

Try testing.

1. Start testing by running "./tests_console/Start-Tests.ps1".
2. Test result will be shown on console. And will also be written to file in "./tests_console/Output/" directory.
3. Play around by changing test or script and running test.

After you get the feeling of the testing, feel free to replace the sample module and its tests for your own module.

If you have Visual Studio Code with Remote - Containers extension, you can try all above in the dev container. Dev container included in this repository comes from https://github.com/microsoft/vscode-dev-containers. Check inside "./.devcontainer/devcontainer.json" for furthur details.

## Features

- Seamlessly write test, write script, run test.
    - If the test loads the script using "using module", script is loaded only once. No reload.
        Which makes "write script, run test, fix script, run test..." impossible.
        The test should load the script using "Import-Module -Force" so that the script is loaded (and reloaded) every time the test runs.
- Don't want to rewrite import commands.
    - Commands for importing the package is packed in "./tests/Fibbonaci/Import-TargetPackage.ps1".
        When you change your package name, change this script, too. No need to change all test scripts.

## Environment

Powershell-TDD-Platform is tested in 3 environments.

One is the dev container included in Powershell-TDD-Platform. 

```powershell
Name                           Value
----                           -----
PSVersion                      7.2.1
PSEdition                      Core
GitCommitId                    7.2.1
OS                             Linux 5.4.72-microsoft-standard-WSL2 #1 SMP Wed Oct 28 23:40:43 UTC 2020
Platform                       Unix
PSCompatibleVersions           {1.0, 2.0, 3.0, 4.0…}
PSRemotingProtocolVersion      2.3
SerializationVersion           1.1.0.1
WSManStackVersion              3.0


    Directory: /home/vscode/.local/share/powershell/Modules

ModuleType Version    PreRelease Name                                PSEdition ExportedCommands
---------- -------    ---------- ----                                --------- ----------------
Script     4.10.1                Pester                              Desk      {Describe, Context, It, Should…}
Script     1.20.0                PSScriptAnalyzer                    Desk      {Get-ScriptAnalyzerRule, Invoke-ScriptAnalyzer, Invoke-Formatter}

    Directory: /home/vscode/.vscode-server/extensions/ms-vscode.powershell-2021.12.0/modules

ModuleType Version    PreRelease Name                                PSEdition ExportedCommands
---------- -------    ---------- ----                                --------- ----------------
Script     1.20.0                PSScriptAnalyzer                    Desk      {Get-ScriptAnalyzerRule, Invoke-ScriptAnalyzer, Invoke-Formatter}
```

Second is my local windows10 PC. Powershell is in preinstalled state, with no modules added. Unlike the container, it does not have PSScriptAnalyzer.

```powershell
Name                           Value
----                           -----
PSVersion                      5.1.19041.1320
PSEdition                      Desktop
PSCompatibleVersions           {1.0, 2.0, 3.0, 4.0...}
BuildVersion                   10.0.19041.1320
CLRVersion                     4.0.30319.42000
WSManStackVersion              3.0
PSRemotingProtocolVersion      2.3
SerializationVersion           1.1.0.1



    ディレクトリ: C:\Program Files\WindowsPowerShell\Modules


ModuleType Version    Name                                ExportedCommands
---------- -------    ----                                ----------------
Script     3.4.0      Pester                              {Describe, Context, It, Should...}
```

Third is GitLab CI.

```powershell
Name                           Value
----                           -----
PSVersion                      5.1.17763.1007
PSEdition                      Desktop
PSCompatibleVersions           {1.0, 2.0, 3.0, 4.0...}
BuildVersion                   10.0.17763.1007
CLRVersion                     4.0.30319.42000
WSManStackVersion              3.0
PSRemotingProtocolVersion      2.3
SerializationVersion           1.1.0.1
    Directory: C:\Program Files (x86)\WindowsPowerShell\Modules
ModuleType Version    Name                                ExportedCommands
---------- -------    ----                                ----------------
Script     3.4.0      Pester                              {Describe, Context, It, Should...}
    Directory: C:\Program Files\WindowsPowerShell\Modules
ModuleType Version    Name                                ExportedCommands
---------- -------    ----                                ----------------
Script     3.4.0      Pester                              {Describe, Context, It, Should...}
Script     1.20.0     PSScriptAnalyzer                    {Get-ScriptAnalyzerRule, Invoke-ScriptAnalyzer, Invoke-For...
```

## Coverage

"./tests_console/Start-Tests.ps1" displays code coverage report like:

```
Code coverage report:
Covered 100.00% of 11 analyzed commands in 2 files.
```

### Set GitLab to collect coverage 

### From GitLab 15.0

You can set GitLab to collect the coverage in .gitlab-ci.yml.

See: ["Add test coverage results using coverage keyword" in GitLab Docs](https://docs.gitlab.com/ee/ci/pipelines/settings.html#add-test-coverage-results-using-coverage-keyword)

### Up to GitLab 14.8

> This feature is in its end-of-life process. It was deprecated in GitLab 14.8. The feature is removed in GitLab 15.0.

You can set GitLab to collect the coverage (to show coverage on badge, for example):

Settings -> CI/CD -> General pipelines -> Test coverage parsing

```
Covered (\d+.\d+%)
```
