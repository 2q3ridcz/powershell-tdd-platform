﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\Import-TargetPackage.ps1")

Describe "Fibbonaci Sequence Calculator" {
    Context "Unit Tests" {
        It "Returns expected values" {
            $result = Get-Fibbonaci -Max 7
            $expect = @(1,1,2,3,5,8,13)

            $result.Count | Should Be $expect.Count
            0..($expect.Count - 1) | %{ $result[$_] | Should Be $expect[$_] }
        }
    }
}
